

class InputData:

    def __init__(self, source, target, target_bpe_tokens, model_scores):
        """
        :param source: str
        :param target: str
        :param target_bpe_tokens: str
        :param model_scores: List[float]
        We assume target_bpe_tokens and model scores include eos token, which is ignored for now,
        but might be useful for a later version
        """
        self.source = source
        self.target = target
        self.target_bpe_tokens = target_bpe_tokens.strip(' </s>')
        self.model_scores = model_scores[:-1]


class Output:

    def __init__(self, sentence_score, word_scores):
        """
        :param word_scores: np.ndarray of shape (num_words,)
        :param sentence_score: float
        """
        self.sentence_score = sentence_score
        self.word_scores = word_scores
        # TODO: optionally, we could also return classifier confidences
