

def map_words_to_bpe_tokens(bpe_tokens, bpe_sep='▁'):
    """
    :param bpe_tokens: str
    :param bpe_sep: bpe separator
    :return: List[Tuple]
    Returns the a list where each element is a tuple containing the indexes of bpe tokens corresponding
    to each target word
    """
    indexes = []
    word = []
    chr_idx = 0
    bpe_idx = 0
    bpe_tokens = bpe_tokens.lstrip(bpe_sep)
    while True:
        if chr_idx == len(bpe_tokens) - 1:
            word.append(bpe_idx)
            indexes.append(tuple(word))
            break
        if bpe_tokens[chr_idx] == bpe_sep:
            indexes.append(tuple(word))
            word = []
        elif bpe_tokens[chr_idx] == ' ':
            word.append(bpe_idx)
            bpe_idx += 1
        else:
            pass
        chr_idx += 1
    return indexes
